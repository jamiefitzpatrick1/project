# project

I have used the Wiki to display my work, and I will use this to update my progress!

There are currently 5 pages:

[My current dissertation write-up](https://gitlab.com/jamiefitzpatrick1/project/-/wikis/Dissertation-write-up)

[My methodology plan](https://gitlab.com/jamiefitzpatrick1/project/-/wikis/Methodology-plan) which will be updated if any changes are needed

[My plan following this submission](https://gitlab.com/jamiefitzpatrick1/project/-/wikis/planning)

[Testing I have done using some tools](https://gitlab.com/jamiefitzpatrick1/project/-/wikis/testing)

[A plan of part of a literature review table](https://gitlab.com/jamiefitzpatrick1/project/-/wikis/literature-review-planning) (to be added to)